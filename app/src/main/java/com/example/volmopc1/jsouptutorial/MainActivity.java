package com.example.volmopc1.jsouptutorial;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity {
    private static final String REGISTER_URL = "http://hellohelps.com/HelloHelps/z_set_news_ent.php";
    private static final String REGISTER_URL1 = "http://hellohelps.com/HelloHelps/z_set_news_india.php";

    public static final String KEY_PAGELINK = "pagelink";
    public static final String KEY_TITLE = "title";
    public static final String KEY_IMAGELINK = "imagelink";
    public static final String KEY_BODY = "body";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

       /* AsyncTaskRunner runner = new AsyncTaskRunner();
        runner.execute();*/

        /* my m = new my();
        thread = new Thread(m);
        thread.start();*/

       /* Timer myTimer = new Timer();
        myTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                Intent intent = new Intent(getApplicationContext(), MyService.class);
                startService(intent);
            }
        }, 0, 3000);*/

       /* Intent intent = new Intent(getApplicationContext(), MyService.class);
        startService(intent);*/

        startService(new Intent(getBaseContext(), MyService1.class));

    }


    private class AsyncTaskRunner extends AsyncTask<String, String, String> {

        private String resp = "tf";


        @Override
        protected String doInBackground(String... params) {

            try {
                getNews1();
                getNews2();
                getNews3();
                getNews4();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return resp;
        }

        @Override
        protected void onPostExecute(String result) {

        }

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected void onProgressUpdate(String... text) {


        }
    }

    class my implements Runnable {
        @Override
        public void run() {

            try {
                getNews1();
                getNews2();
                getNews3();
                getNews4();

            } catch (IOException e) {
                e.printStackTrace();
            }

            Log.d("f","dfgfgfgfgfgfgfgfgfgfgfgfgfg");
//           Toast.makeText(MainActivity.this, "Service Started", Toast.LENGTH_LONG).show();
        }
    }

    public void volleeyy(final String linkHref1, final String title, final String imagelink, final String body) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, REGISTER_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Toast.makeText(MainActivity.this, response, Toast.LENGTH_LONG).show();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(MainActivity.this, error.toString(), Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put(KEY_PAGELINK, linkHref1);
                params.put(KEY_TITLE, title);
                params.put(KEY_IMAGELINK, imagelink);
                params.put(KEY_BODY, body);
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    public void volleeyy1(final String linkHref1, final String title, final String imagelink, final String body) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, REGISTER_URL1,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Toast.makeText(MainActivity.this, response, Toast.LENGTH_LONG).show();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(MainActivity.this, error.toString(), Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put(KEY_PAGELINK, linkHref1);
                params.put(KEY_TITLE, title);
                params.put(KEY_IMAGELINK, imagelink);
                params.put(KEY_BODY, body);
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }


    public void getNews1() throws IOException {
        Document doc = Jsoup.connect("http://www.deccanchronicle.com/entertainment/bollywood").get();
        for (int i = 111; i < 130; i++) {
            int j = (i % 2);
            if (j == 0) {
                Element link = doc.select("a").get(i);
                String linkHref = link.attr("href");
                String s = "http://www.deccanchronicle.com" + linkHref;
                volleeyy(s, getTitle1(s), getImagelink1(s), getBody1(s));//volleyadded
            }
        }
    }

    public String getImagelink1(String i) throws IOException {
        Document doc2 = Jsoup.connect(i).get();
        String imagelink = doc2.select("meta[property=og:image]").get(0).attr("content");
        //System.out.println(imagelink);
        return imagelink;
    }

    public String getTitle1(String t) throws IOException {
        Document doc1 = Jsoup.connect(t).get();
        String title1 = doc1.title();
        //System.out.println(title1);
        return title1;
    }

    public String getBody1(String b) throws IOException {
        String boddy = "";
        try {
            Document doc3 = Jsoup.connect(b).get();

            for (int i = 2; i < 200; i++) {

                Element e = doc3.select("p").get(i);
                String body = e.text();
                boddy = boddy.concat(body);
            }

        } catch (IndexOutOfBoundsException e) {

        }
        return boddy;

    }



    public void getNews2() throws IOException {

        Document doc = Jsoup.connect("http://www.rajnikantvscidjokes.in/category/entertainment/").get();
        for (int i1 = 39; i1 < 80; i1++) {
            int j = (i1 % 2);
            if (j == 0) {
                Element link = doc.select("a").get(i1 + 1);
                String linkHref = link.attr("href");
                volleeyy(linkHref, getTitle2(linkHref), getImagelink2(linkHref), getBody2(linkHref));//volleyadded
            }
        }
    }

    public String getImagelink2(String i) throws IOException{

        Document doc2 = Jsoup.connect(i).get();
        String imagelink = doc2.select("meta[property=og:image]").get(0).attr("content");
        //System.out.println(imagelink);
        return imagelink;
    }

    public String getTitle2(String t)throws IOException{
        Document doc1 = Jsoup.connect(t).get();
        String title1 = doc1.title();
        //System.out.println(title1);
        return title1;
    }

    public String getBody2(String b) throws IOException{
        String boddy="";

        try {
            Document doc3 = Jsoup.connect(b).get();

            for (int i = 2; i < 200; i++) {

                Element e = doc3.select("p").get(i);
                String body = e.text();
                boddy = boddy.concat(body);
            }

        } catch (IndexOutOfBoundsException e) {

        }
        return boddy;

    }


    public void getNews3()  throws IOException{

    Document doc = Jsoup.connect("http://indianexpress.com/section/entertainment/bollywood/").get();
    for (int i = 64; i < 136; i=i+2) {
        Element link = doc.select("a").get(i);
        String linkHref = link.attr("href");
        volleeyy(linkHref, getTitle3(linkHref),getImagelink3(linkHref),getBody3(linkHref));//volleyadded
    }

}

    public String getImagelink3(String i) throws IOException{


        Document doc2 = Jsoup.connect(i).get();
        String imagelink = doc2.select("meta[property=og:image]").get(0).attr("content");
        //System.out.println(imagelink);
        return imagelink;
    }

    public String getTitle3(String t)throws IOException{

        Document doc1 = Jsoup.connect(t).get();
        String title1 = doc1.title();
        //System.out.println(title1);
        return title1;
    }

    public String getBody3(String b) throws IOException{

        String boddy="";

        try {
            Document doc3 = Jsoup.connect(b).get();

            for (int i = 2; i < 200; i++) {

                Element e = doc3.select("p").get(i);
                String body = e.text();
                boddy = boddy.concat(body);
            }

        } catch (IndexOutOfBoundsException e) {

        }
        return boddy;

    }










    //getting the indian news

    public void getNews4()  throws IOException{

        Document doc = Jsoup.connect("http://indianexpress.com/section/india/").get();
        for (int i = 57; i < 137; i=i+2) {
            Element link = doc.select("a").get(i);
            String linkHref = link.attr("href");
            volleeyy1(linkHref, getTitle4(linkHref),getImagelink4(linkHref),getBody4(linkHref));//volleyadded
        }

    }

    public String getImagelink4(String i) throws IOException{


        Document doc2 = Jsoup.connect(i).get();
        String imagelink = doc2.select("meta[property=og:image]").get(0).attr("content");
        //System.out.println(imagelink);
        return imagelink;
    }

    public String getTitle4(String t)throws IOException{

        Document doc1 = Jsoup.connect(t).get();
        String title1 = doc1.title();
        //System.out.println(title1);
        return title1;
    }

    public String getBody4(String b) throws IOException{

        String boddy="";

        try {
            Document doc3 = Jsoup.connect(b).get();

            for (int i = 2; i < 200; i++) {

                Element e = doc3.select("p").get(i);
                String body = e.text();
                boddy = boddy.concat(body);
            }

        } catch (IndexOutOfBoundsException e) {

        }
        return boddy;

    }






}















