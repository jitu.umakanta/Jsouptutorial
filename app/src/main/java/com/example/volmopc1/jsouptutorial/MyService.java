package com.example.volmopc1.jsouptutorial;

import android.app.IntentService;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.provider.DocumentsContract;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class MyService extends IntentService {
    private static final String REGISTER_URL = "http://hellohelps.com/HelloHelps/z_set_news_ent.php";
    private static final String REGISTER_URL1 = "http://hellohelps.com/HelloHelps/z_set_news_india.php";
    public static final String KEY_PAGELINK = "pagelink";
    public static final String KEY_TITLE = "title";
    public static final String KEY_IMAGELINK = "imagelink";
    public static final String KEY_BODY = "body";

    /**
     * A constructor is required, and must call the super IntentService(String)
     * constructor with a name for the worker thread.
     */
    public MyService() {
        super("MyService");
    }

    /**
     * The IntentService calls this method from the default worker thread with
     * the intent that started the service. When this method returns, IntentService
     * stops the service, as appropriate.
     */
    @Override
    protected void onHandleIntent(Intent intent) {
        // Normally we would do some work here, like download a file.
        // For our sample, we just sleep for 5 seconds.

        // try {
           /* getNews1();
            getNews2();
            getNews3();
            getNews6();
            getNews4();*/
       /* try {
            getNews11();
        } catch (IOException e) {
            e.printStackTrace();
        }*/
        String[] a = {"http://www.pinkvilla.com/entertainment", "https://www.sportskeeda.com/cricket", "http://www.dnaindia.com/india", "http://indiatoday.intoday.in/", "http://www.rajnikantvscidjokes.in/"};
        pa(a);

        // } catch (IOException e) {
        // e.printStackTrace();
        //}
        try {
            Thread.sleep(5);
        } catch (InterruptedException e) {
            // Restore interrupt status.
            Thread.currentThread().interrupt();
        }
    }


    public void volleeyy(final String linkHref1, final String title, final String imagelink, final String body) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, REGISTER_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Toast.makeText(getApplicationContext(), response, Toast.LENGTH_LONG).show();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), error.toString(), Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put(KEY_PAGELINK, linkHref1);
                params.put(KEY_TITLE, title);
                params.put(KEY_IMAGELINK, imagelink);
                params.put(KEY_BODY, body);
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    public void volleeyy1(final String linkHref1, final String title, final String imagelink, final String body) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, REGISTER_URL1,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Toast.makeText(getApplicationContext(), response, Toast.LENGTH_LONG).show();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(getApplicationContext(), error.toString(), Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put(KEY_PAGELINK, linkHref1);
                params.put(KEY_TITLE, title);
                params.put(KEY_IMAGELINK, imagelink);
                params.put(KEY_BODY, body);
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }


    public void getNews1() throws IOException {
        Document doc = Jsoup.connect("http://www.deccanchronicle.com/entertainment/bollywood").get();
        for (int i = 111; i < 130; i++) {
            int j = (i % 2);
            if (j == 0) {
                Element link = doc.select("a").get(i);
                String linkHref = link.attr("href");
                String s = "http://www.deccanchronicle.com" + linkHref;
                volleeyy(s, getTitle1(s), getImagelink1(s), getBody1(s));//volleyadded
            }
        }
    }

    public String getImagelink1(String i) throws IOException {
        Document doc2 = Jsoup.connect(i).get();
        String imagelink = doc2.select("meta[property=og:image]").get(0).attr("content");
        //System.out.println(imagelink);
        return imagelink;
    }

    public String getTitle1(String t) throws IOException {
        Document doc1 = Jsoup.connect(t).get();
        String title1 = doc1.title();
        //System.out.println(title1);
        return title1;
    }

    public String getBody1(String b) throws IOException {
        String boddy = "";
        try {
            Document doc3 = Jsoup.connect(b).get();

            for (int i = 2; i < 200; i++) {

                Element e = doc3.select("p").get(i);
                String body = e.text();
                boddy = boddy.concat(body);
            }

        } catch (IndexOutOfBoundsException e) {

        }
        return boddy;

    }


    public void getNews2() throws IOException {

        Document doc = Jsoup.connect("http://www.rajnikantvscidjokes.in/category/entertainment/").get();
        for (int i1 = 39; i1 < 80; i1++) {
            int j = (i1 % 2);
            if (j == 0) {
                Element link = doc.select("a").get(i1 + 1);
                String linkHref = link.attr("href");
                volleeyy(linkHref, getTitle2(linkHref), getImagelink2(linkHref), getBody2(linkHref));//volleyadded
            }
        }
    }

    public String getImagelink2(String i) throws IOException {

        Document doc2 = Jsoup.connect(i).get();
        String imagelink = doc2.select("meta[property=og:image]").get(0).attr("content");
        //System.out.println(imagelink);
        return imagelink;
    }

    public String getTitle2(String t) throws IOException {
        Document doc1 = Jsoup.connect(t).get();
        String title1 = doc1.title();
        //System.out.println(title1);
        return title1;
    }

    public String getBody2(String b) throws IOException {
        String boddy = "";

        try {
            Document doc3 = Jsoup.connect(b).get();

            for (int i = 2; i < 200; i++) {

                Element e = doc3.select("p").get(i);
                String body = e.text();
                boddy = boddy.concat(body);
            }

        } catch (IndexOutOfBoundsException e) {

        }
        return boddy;

    }


    public void getNews3() throws IOException {

        Document doc = Jsoup.connect("http://indianexpress.com/section/entertainment/bollywood/").get();
        for (int i = 64; i < 136; i = i + 2) {
            Element link = doc.select("a").get(i);
            String linkHref = link.attr("href");
            volleeyy(linkHref, getTitle3(linkHref), getImagelink3(linkHref), getBody3(linkHref));//volleyadded
        }

    }

    public String getImagelink3(String i) throws IOException {

        String imagelink = null;
        Document doc2 = Jsoup.connect(i).get();
        try {
            imagelink = doc2.select("meta[property=og:image]").get(0).attr("content");
        } catch (Exception e) {
            imagelink = "http://hellohelps.com/HelloHelps/10.png";
        }

        //System.out.println(imagelink);
        return imagelink;
    }

    public String getTitle3(String t) throws IOException {

        Document doc1 = Jsoup.connect(t).get();
        String title1 = doc1.title();
        //System.out.println(title1);
        return title1;
    }

    public String getBody3(String b) throws IOException {

        String boddy = "";

        try {
            Document doc3 = Jsoup.connect(b).get();

            for (int i = 2; i < 200; i++) {

                Element e = doc3.select("p").get(i);
                String body = e.text();
                boddy = boddy.concat(body);
            }

        } catch (IndexOutOfBoundsException e) {

        }
        return boddy;

    }


    //getting the indian news

    public void getNews4() throws IOException {

        Document doc = Jsoup.connect("http://indianexpress.com/section/india/").get();
        for (int i = 58; i < 137; i = i + 2) {
            Element link = doc.select("a").get(i);
            String linkHref = link.attr("href");
            volleeyy1(linkHref, getTitle4(linkHref), getImagelink4(linkHref), getBody4(linkHref));//volleyadded
        }

    }

    public String getImagelink4(String i) throws IOException {


        Document doc2 = Jsoup.connect(i).get();
        String imagelink = doc2.select("meta[property=og:image]").get(0).attr("content");
        //System.out.println(imagelink);
        return imagelink;
    }

    public String getTitle4(String t) throws IOException {
        Log.d("urrrrrl", t);
        Document doc1 = Jsoup.connect(t).get();
        String title1 = doc1.title();
        //System.out.println(title1);
        return title1;
    }

    public String getBody4(String b) throws IOException {

        String boddy = "";

        try {
            Document doc3 = Jsoup.connect(b).get();

            for (int i = 2; i < 200; i++) {

                Element e = doc3.select("p").get(i);
                String body = e.text();
                boddy = boddy.concat(body);
            }

        } catch (IndexOutOfBoundsException e) {

        }
        return boddy;

    }


    public void getNews6() throws IOException {

        Document doc = Jsoup.connect("http://www.hindustantimes.com/india-news/").get();
        for (int i = 99; i < 190; i = i + 2) {
            Element link = doc.select("a").get(i);
            String linkHref = link.attr("href");
            System.out.println(linkHref);
            System.out.println(getTitle6(linkHref));
            System.out.println(getImagelink6(linkHref));
            System.out.println(getBody6(linkHref));
            volleeyy(linkHref, getTitle6(linkHref), getImagelink6(linkHref), getBody6(linkHref));//volleyadded
        }


    }

    public String getTitle6(String t) throws IOException {

        Document doc1 = Jsoup.connect(t).get();
        String title1 = doc1.title();
        //System.out.println(title1);
        return title1;
    }

    public String getImagelink6(String i) throws IOException {


        Document doc2 = Jsoup.connect(i).get();
        String imagelink = doc2.select("meta[property=og:image]").get(0).attr("content");
        //System.out.println(imagelink);
        return imagelink;
    }

    public String getBody6(String b) throws IOException {

        String boddy = "";

        try {
            Document doc3 = Jsoup.connect(b).get();

            for (int i = 2; i < 200; i++) {

                Element e = doc3.select("p").get(i);
                String body = e.text();
                boddy = boddy.concat(body);
            }

        } catch (IndexOutOfBoundsException e) {

        }
        return boddy;

    }


    public void getNews8() throws IOException {

        Document doc = Jsoup.connect("http://www.thehindu.com/news/national/").get();
        for (int i = 429; i < 483; i = i + 2) {
            Element link = doc.select("a").get(i);
            String linkHref = link.attr("href");
            //  System.out.println(linkHref);
            Log.d("link", linkHref);
            // System.out.println(getTitle6(linkHref));
            Log.d("title", getTitle8(linkHref));
            // System.out.println(getImagelink6(linkHref));
            //Log.d("imagelink",getImagelink7(linkHref));
            // Log.d("body",getBody7(linkHref));
            //System.out.println(getBody6(linkHref));
            //volleeyy(linkHref, getTitle5(linkHref), getImagelink5(linkHref), getBody5(linkHref));//volleyadded
        }


    }

    public String getTitle8(String t) throws IOException {

        String title1 = null;

        try {
            Document doc1 = Jsoup.connect(t).get();
            title1 = doc1.title();
        } catch (Exception e) {
            title1 = "fff";
        }
        //System.out.println(title1);
        return title1;
    }


    public void getNews9() throws IOException {

        Document doc = Jsoup.connect("http://www.oneindia.com/news/").get();
        for (int i = 120; i < 146; i = i + 2) {
            Element link = doc.select("a").get(i);
            String linkHref = link.attr("href");
            //  System.out.println(linkHref);
            Log.d("link", linkHref);
            // System.out.println(getTitle6(linkHref));
            // Log.d("title",getTitle8(linkHref));
            // System.out.println(getImagelink6(linkHref));
            //Log.d("imagelink",getImagelink7(linkHref));
            // Log.d("body",getBody7(linkHref));
            //System.out.println(getBody6(linkHref));
            //volleeyy(linkHref, getTitle5(linkHref), getImagelink5(linkHref), getBody5(linkHref));//volleyadded
        }


    }

    public void getNews10() throws IOException {

        Document doc = Jsoup.connect("http://www.deccanherald.com/contents/70/national.html").get();
        for (int i = 75; i < 144; i = i + 1) {
            Element link = doc.select("a").get(i);
            String linkHref = link.attr("href");
            //  System.out.println(linkHref);
            Log.d("link", linkHref);
            Log.d("title", getTitle10(linkHref));
            Log.d("imagelink", getImagelink10(linkHref));
            Log.d("body", getBody10(linkHref));
            //volleeyy(linkHref, getTitle5(linkHref), getImagelink5(linkHref), getBody5(linkHref));//volleyadded
        }


    }

    public String getTitle10(String t) throws IOException {

        Document doc1 = Jsoup.connect(t).get();
        String title1 = doc1.title();
        //System.out.println(title1);
        return title1;
    }

    public String getImagelink10(String i) throws IOException {


        Document doc2 = Jsoup.connect(i).get();
        String imagelink = doc2.select("meta[property=og:image]").get(0).attr("content");
        //System.out.println(imagelink);
        return imagelink;
    }

    public String getBody10(String b) throws IOException {

        String boddy = "";

        try {
            Document doc3 = Jsoup.connect(b).get();

            for (int i = 2; i < 200; i++) {

                Element e = doc3.select("p").get(i);
                String body = e.text();
                boddy = boddy.concat(body);
            }

        } catch (IndexOutOfBoundsException e) {

        }
        return boddy;

    }


    public void pa(String[] a) {
        String url;
        for (int i = 0; i < a.length; i++) {
            url = a[i];
            try {
                getNews11(url);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    public void getNews11(String url) throws IOException {
        // String url = "http://www.pinkvilla.com/entertainment";
        Document doc = Jsoup.connect(url).get();
        try {
            for (int i = 10; i < 700; i = i + 1) {
                Element link = doc.select("a").get(i);
                String linkHref = link.attr("href");
                String linkHref1;
                if (linkHref.contains("www")) {
                    linkHref1 = linkHref;
                } else {
                    linkHref1 = url + linkHref;
                }
                if (getBody11(linkHref1).contains("nobody") || getBody11(linkHref1).contains("Loading") || getImagelink11(linkHref1).contains("noimage")) {


                } else {
                    Log.d("link", linkHref1);
                    Log.d("title", getTitle11(linkHref1));
                    //Log.d("imagelink", getImagelink11(linkHref1));
                    //Log.d("body", getBody11(linkHref1));
                }
                //volleeyy(linkHref, getTitle5(linkHref), getImagelink5(linkHref), getBody5(linkHref));//volleyadded
            }
        } catch (IndexOutOfBoundsException e) {
            Log.d("enddddd", "enddddd");
        }


    }

    public String getTitle11(String t) throws IOException {
        String title1 = "notitle";
        String firstLetter = String.valueOf(t.charAt(0));
        if (firstLetter.equals("h")) {
            Document doc1 = Jsoup.connect(t).get();
            try {
                title1 = doc1.title();
                //System.out.println(title1);
            } catch (Exception e) {
                Log.d("sss", "titleee");
            }
        }
        return title1;
    }

    public String getImagelink11(String i) throws IOException {
        String imagelink = "noimagee";

        Document doc2 = Jsoup.connect(i).get();
        try {
            imagelink = doc2.select("meta[property=og:image]").get(0).attr("content");
        } catch (Exception e) {
            //imagelink="kkk";
        }
        //System.out.println(imagelink);
        return imagelink;
    }

    public String getBody11(String b) throws IOException {

        String boddy = "";

        try {
            Document doc3 = Jsoup.connect(b).get();

            for (int i = 2; i < 200; i++) {

                Element e = doc3.select("p").get(i);
                String body = e.text();
                boddy = boddy.concat(body);
            }

        } catch (Exception e) {
            //boddy=boddy.concat("nobody");
            if (boddy.trim().equals("")) {
                boddy = boddy.concat("nobody");
            }

        }
        return boddy;

    }

}
